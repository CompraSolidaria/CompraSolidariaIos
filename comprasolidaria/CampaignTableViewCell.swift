//
//  CampaignTableViewCell.swift
//  comprasolidaria
//
//  Created by user129281 on 7/21/17.
//  Copyright © 2017 Compra Solidaria. All rights reserved.
//

import UIKit

class CampaignTableViewCell: UITableViewCell {
    
    @IBOutlet weak var campaignBanner: UIImageView!
    @IBOutlet weak var campaignTitle: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
