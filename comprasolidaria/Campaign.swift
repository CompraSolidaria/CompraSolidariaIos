//
//  Campaign.swift
//  comprasolidaria
//
//  Created by user129281 on 7/24/17.
//  Copyright © 2017 Compra Solidaria. All rights reserved.
//

import Foundation

class Campaign {
    
    var id: String
    var name: String
    var description: String
    var votes: String
    var pictures: [Picture]?
    
    init(_ id: String, _ name: String, _ description: String, _ votes: String) {
        self.id = id
        self.name = name
        self.description = description
        self.votes = votes
    }
}
