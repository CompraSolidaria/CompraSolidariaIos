//
//  CampaignAPI.swift
//  comprasolidaria
//
//  Created by user129281 on 7/24/17.
//  Copyright © 2017 Compra Solidaria. All rights reserved.
//

import Foundation
import UIKit

class CampaignAPI {
    
    static let basePath = "http://localhost:3000"
    
    // Cria conexão com configurações personalizadas
    static let config: URLSessionConfiguration = {
        let config = URLSessionConfiguration.default
        
        config.allowsCellularAccess = false
        config.httpAdditionalHeaders = [
            "Content-type": "application/json"
        ]
        config.timeoutIntervalForRequest = 30.0
        config.httpMaximumConnectionsPerHost = 1
        
        return config
    }()
    
    static let session = URLSession(configuration: config)
    
    // GET
    static func campaignEndedList(date: String, onComplete: @escaping ([Campaign]?) -> Void) {
        
        guard let url = URL(string: "\(basePath)/campaigns/search/ended/\(date)") else {
            onComplete(nil)
            return
        }
        
        var campaigns: [Campaign] = []
        
        campaigns.append(Campaign("201706_1", "Campanha FEAC", "Campanha FEAC", "5"))
        campaigns.append(Campaign("201706_2", "Campanha Itaú", "Campanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\nCampanha Itaú\n", "3"))
        
        onComplete(campaigns)
        /*
        session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil {
                onComplete(nil)
            } else {
                guard let response = response as? HTTPURLResponse else {
                    onComplete(nil)
                    return
                }
                
                if response.statusCode == 200 {
                    guard let data = data else {
                        onComplete(nil)
                        return
                    }
                    
                    let json = try! JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! [[String: Any]]
                    
                    var campaigns: [Campaign] = []
                    
                    for item in json {
                        guard let id = item["id"] as! String? else { return }
                        guard let name = item["name"] as! String? else { return }
                        guard let description = item["description"] as! String? else { return }
                        guard let votes = item["votes"] as! String? else { return }
                        let campaign = Campaign(id, name, description, votes)
                        campaigns.append(campaign)
                    }
                    onComplete(campaigns)
                    return
                } else {
                    onComplete(nil)
                    return
                }
            }
        }.resume()
        */
    }
    
    static func campaignPicturesList(campaignId: String, onComplete: @escaping ([Picture]?) -> Void) {
        guard let url = URL(string: "\(basePath)/campaigns/\(campaignId)/pictures") else {
            onComplete(nil)
            return
        }
        
        var pictures: [Picture] = []
        
        pictures.append(Picture("1", "http://www.feac.org.br/wp-content/uploads/2016/08/voluntariado_2.gif"))
        pictures.append(Picture("0", "http://www.feac.org.br/wp-content/uploads/2017/07/Hip-hop-leva-determina%C3%A7%C3%A3o-valores-e-possibilidades-para-crian%C3%A7as-e-adolescentes-na-Semear-820x400.jpg"))
        pictures.append(Picture("0", "http://www.feac.org.br/wp-content/uploads/2017/07/Hip-hop-leva-determina%C3%A7%C3%A3o-valores-e-possibilidades-para-crian%C3%A7as-e-adolescentes-na-Semear-820x400.jpg"))
        pictures.append(Picture("0", "http://www.feac.org.br/wp-content/uploads/2017/07/Hip-hop-leva-determina%C3%A7%C3%A3o-valores-e-possibilidades-para-crian%C3%A7as-e-adolescentes-na-Semear-820x400.jpg"))
        pictures.append(Picture("0", "http://www.feac.org.br/wp-content/uploads/2017/07/Hip-hop-leva-determina%C3%A7%C3%A3o-valores-e-possibilidades-para-crian%C3%A7as-e-adolescentes-na-Semear-820x400.jpg"))
        pictures.append(Picture("0", "http://www.feac.org.br/wp-content/uploads/2017/07/Hip-hop-leva-determina%C3%A7%C3%A3o-valores-e-possibilidades-para-crian%C3%A7as-e-adolescentes-na-Semear-820x400.jpg"))
        pictures.append(Picture("0", "http://www.feac.org.br/wp-content/uploads/2017/07/Hip-hop-leva-determina%C3%A7%C3%A3o-valores-e-possibilidades-para-crian%C3%A7as-e-adolescentes-na-Semear-820x400.jpg"))
        pictures.append(Picture("0", "http://www.feac.org.br/wp-content/uploads/2017/07/Hip-hop-leva-determina%C3%A7%C3%A3o-valores-e-possibilidades-para-crian%C3%A7as-e-adolescentes-na-Semear-820x400.jpg"))
        
        onComplete(pictures)
        
        /*
        session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil {
                onComplete(nil)
            } else {
                guard let response = response as? HTTPURLResponse else {
                    onComplete(nil)
                    return
                }
                
                if response.statusCode == 200 {
                    guard let data = data else {
                        onComplete(nil)
                        return
                    }
                    
                    let json = try! JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions()) as! [[String: Any]]
                    
                    var pictures: [Picture] = []
                    
                    for item in json {
                        guard let banner = item["banner"] as! String? else { return }
                        guard let url = item["url"] as! String? else { return }
                        let picture = Picture(banner, url)
                        pictures.append(picture)
                    }
                    onComplete(pictures)
                    return
                } else {
                    onComplete(nil)
                    return
                }
            }
        }.resume()
        */
    }
    
    static func downloadPicture(pictureUrl: String, onComplete: @escaping (UIImage?) -> Void) {
        guard let url = URL(string: pictureUrl) else {
            onComplete(nil)
            return
        }
        
        session.dataTask(with: url) { (data: Data?, response: URLResponse?, error: Error?) in
            if error != nil {
                onComplete(nil)
            } else {
                guard let response = response as? HTTPURLResponse else {
                    onComplete(nil)
                    return
                }
                
                if response.statusCode == 200 {
                    guard let data = data else {
                        onComplete(nil)
                        return
                    }
                    
                    onComplete(UIImage(data: data))
                    return
                } else {
                    onComplete(nil)
                    return
                }
            }
        }.resume()
    }

}
