//
//  ResultMonthsTableViewController.swift
//  comprasolidaria
//
//  Created by user129281 on 7/21/17.
//  Copyright © 2017 Compra Solidaria. All rights reserved.
//

import UIKit

class ResultMonthsTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let item = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = item
        
        tableView.tableFooterView = UIView()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "resultMonthCell", for: indexPath)
        
        var date = Date()
        
        date = Calendar.current.date(byAdding: .month, value: ((indexPath.row + 1) * -1), to: date)!
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM (yyyy)"

        cell.textLabel?.textAlignment = .center
        cell.textLabel?.text = dateFormatter.string(from: date)

        return cell
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ResultCampaignsTableViewController {
            let date = Date()
            vc.selectedMonth = Calendar.current.date(byAdding: .month, value: ((tableView.indexPathForSelectedRow!.row + 1) * -1), to: date)!
        }
    }
}
