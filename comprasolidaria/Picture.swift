//
//  Picture.swift
//  comprasolidaria
//
//  Created by user129281 on 7/24/17.
//  Copyright © 2017 Compra Solidaria. All rights reserved.
//

import Foundation

class Picture {
    
    var banner: String
    var url: String
    
    init(_ banner: String, _ url: String) {
        self.banner = banner
        self.url = url
    }
}
