//
//  CampaignViewController.swift
//  comprasolidaria
//
//  Created by user129281 on 7/24/17.
//  Copyright © 2017 Compra Solidaria. All rights reserved.
//

import UIKit

class CampaignViewController: UIViewController {

    var campaign: Campaign?
    
    @IBOutlet weak var label_campaignVotes: UILabel!
    @IBOutlet weak var iv_campaignBanner: UIImageView!
    @IBOutlet weak var tv_campaignDescription: UITextView!
    @IBOutlet weak var pictureCollectionView: UICollectionView!
    @IBOutlet weak var ivPictureZoom: UIImageView!
    @IBOutlet weak var ivPictureZoomView: UIView!
    
    @IBOutlet var tapZoom: UITapGestureRecognizer!
    @IBOutlet var tapZoomView: UITapGestureRecognizer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tapZoom.delegate = self
        tapZoomView.delegate = self
        
        ivPictureZoomView.addGestureRecognizer(tapZoomView)
        ivPictureZoom.addGestureRecognizer(tapZoom)
        
        let item = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = item

        if campaign != nil {
            self.navigationItem.title = campaign?.name
            
            label_campaignVotes.text = "\(campaign?.votes ?? "N/A") Voto(s)"
            tv_campaignDescription.text = campaign?.description
            CampaignAPI.downloadPicture(pictureUrl: (campaign?.pictures?[0].url)!, onComplete: { (banner: UIImage?) in
                DispatchQueue.main.async {
                    self.iv_campaignBanner.image = banner
                }
            })
            
            pictureCollectionView.delegate = self
            pictureCollectionView.dataSource = self
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension CampaignViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // Set the number of items in your collection view.
        return (campaign?.pictures?.count)! - 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = pictureCollectionView.dequeueReusableCell(withReuseIdentifier: "pictureViewCell", for: indexPath) as! PictureCollectionViewCell
        // Do any custom modifications you your cell, referencing the outlets you defined in the Custom cell file.
        
        CampaignAPI.downloadPicture(pictureUrl: (campaign?.pictures?[indexPath.row + 1].url)!, onComplete: { (banner: UIImage?) in
            DispatchQueue.main.async {
                cell.ivPicture.image = banner
            }
        })
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! PictureCollectionViewCell
        ivPictureZoom.image = cell.ivPicture.image
        ivPictureZoomView.isHidden = false
        ivPictureZoom.isHidden = false
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 150.0, height: 75.0)
    }
}

extension CampaignViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if (ivPictureZoomView.isHidden == false) {
            ivPictureZoom.isHidden = true
            ivPictureZoomView.isHidden = true
        }
        
        return false
    }
}
