//
//  ResultCampaignsTableViewController.swift
//  comprasolidaria
//
//  Created by user129281 on 7/21/17.
//  Copyright © 2017 Compra Solidaria. All rights reserved.
//

import UIKit

class ResultCampaignsTableViewController: UITableViewController {
    
    var selectedMonth: Date?
    let cellSpacingHeight: CGFloat = 10
    
    var dataSource: [Campaign] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if selectedMonth != nil {
            let dateFormatter = DateFormatter()
            
            dateFormatter.dateFormat = "yyyy/MM"
            CampaignAPI.campaignEndedList(date: dateFormatter.string(from: selectedMonth!), onComplete: { (campaigns: [Campaign]?) in
                if let campaigns = campaigns {
                    self.dataSource = campaigns
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            })
            
            dateFormatter.dateFormat = "MM/yyyy"
            
            self.navigationItem.title = "Campanhas de (\(dateFormatter.string(from: selectedMonth!)))"
        }

        let item = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem = item
        
        tableView.estimatedRowHeight = 120
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return dataSource.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "resultCampaignCell", for: indexPath) as! CampaignTableViewCell
        let campaign = dataSource[indexPath.section]
        cell.campaignTitle.text = nil
        cell.campaignBanner.image = getImageWithColor(color: .white, size: CGSize(width: 50, height: 120))
        
        cell.campaignTitle.text = campaign.name
        CampaignAPI.campaignPicturesList(campaignId: campaign.id) { (pictures: [Picture]?) in
            campaign.pictures = pictures
            CampaignAPI.downloadPicture(pictureUrl: (campaign.pictures?[0].url)!, onComplete: { (banner: UIImage?) in
                DispatchQueue.main.async {
                    cell.campaignBanner.image = banner
                }
            })
        }

        return cell
    }

    // Set the spacing between sections
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
    
    // Make the background color show through
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? CampaignViewController {
            vc.campaign = dataSource[tableView.indexPathForSelectedRow!.section]
        }
    }
 
    
    // MARK: - Auxiliary
    
    func getImageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }

}
